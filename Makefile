wd=/Your_Code_Dir
sif=/Your_Container_Dir
data=/Your_Raw_Data_Dir
data2=/Your_Output_Dir
tm=$$(date '+%Y-%m-%d_%H-%M-%S')

mainanalysiscontainer:
	singularity exec \
		--bind ${data} \
		--bind ${data2} \
		${sif} \
		R -e "library(knitr); rmarkdown::render('${wd}/PNASPaper_Figures_and_StatTests.Rmd',~+~~+~encoding~+~=~+~'UTF-8')" 

nanostringcombined:
	singularity exec \
		--bind ${data} \
		--bind ${data2} \
		${sif} \
		R -e "library(knitr); rmarkdown::render('${wd}/PNASPaper_NanoStringRNAnCounterData_analysis.Rmd',~+~~+~encoding~+~=~+~'UTF-8')"
