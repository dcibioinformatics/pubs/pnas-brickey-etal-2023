# PNAS-Brickey-etal-2023

## The raw data is available on the ImmPort: 
- Find the data via the link: https://immport.org/shared/study/SDY2533 (Or search the study ID SDY2533 on the main page https://www.immport.org/home and click on the first searching results);
- Download and unzip the PNASPaper_RawData.zip file;
- Check the md5sums of all fils according to the rawdata_md5sum.txt file in the unzipped folder;
- Link to the unzipped folder as the `raw data directory` to run the codes.

## Main codes 
- Makefile 
	- mainanalysiscontainer: main analysis within container
	- nanostringcombined: NanoString analysis within container
- Plots, statistical tests, supplementary tables, and power statement: 
	- PNASPaper_Figures_and_StatTests.Rmd
	- PNASPaper_Functions.R (Function scripts)
- Nanostring nCounter RNA sequencing data pre-processing and DEG analysis: 
	- PNASPaper_NanoStringRNAnCounterData_analysis.Rmd

## Run codes
- Edit the directory paths in PNASPaper_Figures_and_StatTests.Rmd 
	- Lines 38-47 
		- edit if you would like to run interactively 
		- ignore them if you would run Rmd codes via command lines or make file.
	- Lines 64-67 
		- `raw data directory`
		- output directories: one for test and plot raw outputs, one for plots revealing model details, and one for final stat result tables
		- the directory where you put Rmd codes and the PNASPaper_Functions.R (Function scripts)
- Edit the directory paths in PNASPaper_NanoStringRNAnCounterData_analysis.Rmd
	- Lines 58-59
		- `raw data directory`
		- output directory
- If you work with Linux system and can run within singularity container: Run the Makefile
	- Edit the paths in the Makefile
	- cd <Your_Code_Dir>
	- make 
- If Makefile does not work for your system, you can run the two Rmd codes directly after editing the paths. 
- The statisitcal outputs may not be 100% consistent with the output in the html files due to system and package version differences.